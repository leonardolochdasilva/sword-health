const k8s = require('@kubernetes/client-node');
const kc = new k8s.KubeConfig();
kc.loadFromDefault();

const k8sApi = kc.makeApiClient(k8s.CoreV1Api);

const listPods = async (req, res) => {
    console.log("listPods func")
    const namespace = req.query.namespace

    if (bucketName == undefined) return res.status(422).send({ message: "Need pass namespace"});

    try {
        var response = await k8sApi.listNamespacedPod(namespace)
        var podNames = []
        response.body && response.body.items.forEach(
            (element, index, array) => {
                podNames.push(element.metadata.name)
            }
        )
        res.status(200).send(podNames);
    } catch (err){
        err.statusCode == 403 ? res.status(403).send({message: "Pod does not have sufficient permissions"}) : res.status(500).send({message: err.message});
        console.log(err)
    }
}

module.exports = {listPods}