// Imports the Google Cloud client library
const {PubSub} = require('@google-cloud/pubsub');

// Creates a client; cache this for further use
const pubSubClient = new PubSub();

const subscriptionPath = process.env.PUBSUB_SUBSCRIPTION_PATH || "fake-subscriptionPath";

function listenForMessages() {

  // References an existing subscription
  var consumedMessages  = []

  const subscription = pubSubClient.subscription(subscriptionPath);

  const messageHandler = message => {
    console.log(`Received message ${message.id}:`);
    console.log(`\tData: ${message.data}`);
    console.log(`\tAttributes: ${message.attributes}`);
    consumedMessages.push(message.data.toString())
    // "Ack" (acknowledge receipt of) the message
    message.ack();
  };

  // Listen for new messages until timeout is hit
  subscription.on('message', messageHandler);
  return consumedMessages
}

var consumedMessages = listenForMessages()

const listMessages = (req, res) => {
  console.log("listMessages func")
  console.log(consumedMessages)
  res.status(200).send({ message: consumedMessages });  
}

module.exports = {listMessages};