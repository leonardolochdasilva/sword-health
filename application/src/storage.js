const {Storage} = require('@google-cloud/storage');

// Instantiate a storage client
const storage = new Storage();

// A bucket is a container for objects (files).
BUCKET_NAME_LIST_GET = process.env.BUCKET_NAME_LIST_GET || "fake-buket-list-get"
BUCKET_NAME_WRITE = process.env.BUCKET_NAME_WRITE || "fake-buket-write"

const bucketListGet = storage.bucket(BUCKET_NAME_LIST_GET);
const bucketWrite = storage.bucket(BUCKET_NAME_WRITE);

var buckets = {}
buckets[BUCKET_NAME_LIST_GET] = bucketListGet 
buckets[BUCKET_NAME_WRITE] =  bucketWrite

const listFiles = async (req, res) => {
    console.log("listFiles func")
    
    bucketName = req.query.bucketName

    fileName = req.query.fileName

    if (bucketName == undefined) return res.status(422).send({ message: "Need pass bucket name"});
    var bucket = buckets[bucketName]

    if (bucket == undefined) return res.status(422).send({ message: "This bucket name was not create for application"});

    try {
        const [files] = await bucket.getFiles( fileName ? {prefix: fileName} : {});
        let fileInfos = [];

        files.forEach((file) => {
        fileInfos.push({
            name: file.name,
            url: file.metadata.mediaLink,
        });
        });

        res.status(200).send(fileInfos);
    } catch (err) {
        statusCode = err.response.statusCode == 403 ? err.response.statusCode : 500
        res.status(statusCode).send({
        message: "Unable to read list of files!: "+ err.message,
        });
    }
};
  
const createFile = (req, res) => {
    console.log("createFile func")
    bucketName = req.query.bucketName

    if (bucketName == undefined) return res.status(422).send({ message: "Need pass bucket name"});


    resquestContent = req.body
    console.log("Request Content: " + resquestContent.fileName)

    const fileName = resquestContent.fileName
    const fileContent = resquestContent.fileContent
    bucket = buckets[bucketName]

    if (bucket == undefined) return res.status(422).send({ message: "This bucket name was not create for application"});
 
    const blob = bucket.file(fileName)
    const blobStream = blob.createWriteStream({resumable: false});
  
    blobStream.on("error", (err) => {
      statusCode = err.response.statusCode == 403 ? err.response.statusCode : 500
      res.status(statusCode).send({ message: err.message });
    });
  
    blobStream.on("finish", (err) => {
      res.status(200).send({ message: "saved" });
    });
    blobStream.end(fileContent);
};


module.exports = {listFiles, createFile}