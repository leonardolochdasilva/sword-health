const express = require('express');
const { listPods } = require('./src/kubernetes');
const { listMessages } = require('./src/pubsub');
const { listFiles, createFile} = require('./src/storage');

const app = express();
app.use(express.json());


app.get('/health-check', (req, res) => {
  res.status(200).send({ message: "Health!!!" });
});

// PubSub
app.get('/messages', listMessages);

// GCP Storage
app.get("/files", listFiles);
app.post("/files", createFile);

// Kubernetes
app.get("/pods", listPods)

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
  console.log(`App listening on port ${PORT}`);
  console.log('Press Ctrl+C to quit.');
});