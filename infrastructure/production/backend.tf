terraform {
  backend "gcs" {
    bucket = "terraform-states-15069" # <<< Terraform state bucket
    prefix = "production/"
  }
}
