##################################################################################
################################ APIS ############################################
// Allows management of enabled API services for an existing Google Cloud
// Platform project. Services in an existing project that are not defined in
// the config will be removed.
locals {
  allowed_apis = [
    "cloudresourcemanager.googleapis.com",
    "compute.googleapis.com",
    "container.googleapis.com",
    "artifactregistry.googleapis.com"
  ]
}

resource "google_project_service" "apis" {
  project                    = var.project_id
  disable_on_destroy         = true
  disable_dependent_services = true

  for_each = toset(local.allowed_apis)
  service  = each.key
}

##################################################################################
#############################  VPC  ##############################################
locals {
  gke_cluster_subnet_secondery_range_name = {
    pods : "pods"
    services : "services"
  }
  gke_cluster_subnet_name = "gke-cluster"
  vpc_ip                  = "10.0.0.0/8"
}

module "vpc" {
  source       = "terraform-google-modules/network/google"
  version      = "~> 3.0"
  project_id   = var.project_id
  network_name = "vpc"
  routing_mode = "GLOBAL"
  subnets = [
    {
      subnet_name   = "subnet-01"
      subnet_ip     = "10.10.10.0/24"
      subnet_region = var.region
    },
    {
      subnet_name   = local.gke_cluster_subnet_name
      subnet_ip     = "10.10.11.0/24"
      subnet_region = var.region
    }
  ]

  secondary_ranges = {
    subnet-01 = []
    gke-cluster = [
      {
        range_name    = local.gke_cluster_subnet_secondery_range_name.pods
        ip_cidr_range = "10.0.0.0/19"
      },

      {
        range_name    = local.gke_cluster_subnet_secondery_range_name.services
        ip_cidr_range = "10.0.32.0/22"
      }
    ]
  }

  firewall_rules = [
    {
      name      = "allow-ssh-ingress"
      direction = "INGRESS"
      ranges    = ["0.0.0.0/0"]
      allow = [{
        protocol = "tcp"
        ports    = ["22"]
      }]
    },
    {
      name      = "allow-http-ingress"
      direction = "INGRESS"
      ranges    = ["0.0.0.0/0"]
      allow = [{
        protocol = "tcp"
        ports    = ["80"]
      }]
      }, {
      name      = "allow-https-ingress"
      direction = "INGRESS"
      ranges    = ["0.0.0.0/0"]
      allow = [{
        protocol = "tcp"
        ports    = ["443"]
      }]
    },
    {
      name      = "allow-tcp-egress"
      direction = "EGRESS"
      ranges    = ["0.0.0.0/0"]
      allow = [{
        protocol = "tcp"
        ports    = null
      }]
    },
  ]
  routes     = []
  depends_on = [resource.google_project_service.apis]
}

##################################################################################
############################## GKE ###############################################
module "gke" {
  source                             = "terraform-google-modules/kubernetes-engine/google"
  kubernetes_version                 = "1.20.10-gke.1600"
  project_id                         = var.project_id
  name                               = "gke-cluster"
  region                             = var.region
  zones                              = ["us-west2-a", "us-west2-b", "us-west2-c"]
  network                            = module.vpc.network_name
  subnetwork                         = local.gke_cluster_subnet_name
  http_load_balancing                = true
  horizontal_pod_autoscaling         = true
  network_policy                     = true
  create_service_account             = true
  grant_registry_access              = true
  enable_network_egress_export       = true
  enable_resource_consumption_export = true
  enable_vertical_pod_autoscaling    = true
  ip_range_pods                      = local.gke_cluster_subnet_secondery_range_name.pods
  ip_range_services                  = local.gke_cluster_subnet_secondery_range_name.services
  node_pools = [
    {
      name               = "worker-nodes"
      machine_type       = "e2-medium"
      min_count          = 1
      max_count          = 10
      local_ssd_count    = 0
      disk_size_gb       = 10
      disk_type          = "pd-standard"
      image_type         = "COS"
      auto_repair        = true
      auto_upgrade       = true
      initial_node_count = 1
    },
  ]
  node_pools_labels = {
    all = {}
    default-node-pool = {
      default-node-pool = true
    }
  }
  node_pools_metadata = {
    all = {}
    default-node-pool = {
      node-pool-metadata-custom-value = "gke-node-pool"
    }
  }
  master_authorized_networks = [
    {
      cidr_block   = local.vpc_ip
      display_name = "Internal Services"
    },
    {
      cidr_block   = var.master_node_cluster_gke_allow_ip
      display_name = "External Access"
    }
  ]
  node_pools_tags = {
    all = []
    default-node-pool = [
      "default-node-pool",
    ]
  }
  depends_on = [module.vpc, resource.google_project_service.apis]
}

# Pods - GCR Access 
resource "google_project_iam_member" "cluster_service_account-get-image" {
  project = var.project_id
  role    = "roles/containerregistry.ServiceAgent"
  member  = "serviceAccount:${module.gke.service_account}"
}

##################################################################################
################################ APPLICATION #####################################

# Service Account
locals {
  application_name = "sword-health"
}

resource "google_service_account" "application_sa" {
  project      = var.project_id
  account_id   = local.application_name
  display_name = "Application Service Account"
  depends_on   = [resource.google_project_service.apis]
}


# Buckets
module "gcs_bucket" {
  source            = "terraform-google-modules/cloud-storage/google"
  version           = "~> 2.2"
  project_id        = var.project_id
  names             = ["just-list-get", "just-write"]
  prefix            = local.application_name
  randomize_suffix  = true
  set_viewer_roles  = true
  set_creator_roles = true
  versioning = {
    first = true
  }
  bucket_viewers = {
    just-list-get = "serviceAccount:${google_service_account.application_sa.email}"
  }
  bucket_creators = {
    just-write = "serviceAccount:${google_service_account.application_sa.email}"
  }
  # force_destroy is just for the test case. 
  force_destroy = {
    just-write    = true
    just-list-get = true
  }
  depends_on = [resource.google_project_service.apis]
}

# PubSub
module "pubsub" {
  source     = "terraform-google-modules/pubsub/google"
  version    = "~> 3.0.0"
  topic      = "sword-health"
  project_id = var.project_id
  pull_subscriptions = [
    {
      name                    = "subscriptions"
      ack_deadline_seconds    = 20
      max_delivery_attempts   = 5
      maximum_backoff         = "600s"
      minimum_backoff         = "300s"
      enable_message_ordering = true
    }
  ]
  depends_on = [resource.google_project_service.apis, resource.google_service_account.application_sa]
}

resource "google_pubsub_subscription_iam_binding" "application" {
  project      = var.project_id
  subscription = module.pubsub.subscription_paths[0]
  role         = "roles/pubsub.subscriber"
  members = [
    "serviceAccount:${google_service_account.application_sa.email}"
  ]
}

# Binding SA GCP with SA K8S, to see more: https://cloud.google.com/kubernetes-engine/docs/how-to/workload-identity
resource "google_service_account_iam_binding" "application" {
  service_account_id = google_service_account.application_sa.name
  role               = "roles/iam.workloadIdentityUser"
  members            = ["serviceAccount:${var.project_id}.svc.id.goog[kube-system/sword-health]"]
  depends_on = [
    module.gke.cluster_id
  ]
}
##################################################################################
