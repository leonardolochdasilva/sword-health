provider "google" {
  region = "us-west2"
}

provider "google-beta" {
  region = "us-west2"
}
