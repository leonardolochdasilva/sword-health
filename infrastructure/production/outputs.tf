output "application_buckets_names" {
  value = module.gcs_bucket.names_list
}

output "application_service_account_email" {
  value = google_service_account.application_sa.email
}

output "application_pubsub_subscription_paths" {
  value = module.pubsub.subscription_paths
}