variable "project_id" {
  type        = string
  description = "The project ID to manage the resources"
}

variable "region" {
  type        = string
  description = "The region that the resources will be created."
  default     = "us-west2"
}

variable "master_node_cluster_gke_allow_ip" {
  type        = string
  description = "The ip that you would like to add to allow masde node access."
  default     = "0.0.0.0/0"
}

