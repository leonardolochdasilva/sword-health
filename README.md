# Sword Health

# Install Dependencies 

Terraform: sudo make install-terraform

GCloud SDK: sudo make install-gcp-sdk

# REQUIREMENTS
PYTHON > 3.5 

DOCKER

HELM > 3.7.0

KUBECTL

## Overview
# Folders Architecture
The projects have two major folders, infrastructure, and application.
* **Infrastructure**: It is where the terraform files are saved and where the infrastructure logic is.
* **Application**: Local where the application file and halm chart are.
* **Tests**: All test was creates using python language, and this way automate the tests.

# Infrastructure
The challenge was create in Google Cloud. and to create was utilized terraform as IaC(Infrastructure as Code).

The gcp project was not created by terraform because it needs either the organization id or the id of a folder in which you want the project to be created, which also needs to have an organization. To validate an organization on Google Cloud you need to validate a domain, and for the test a domain was not created.

`
Create a project in the Google Cloud Console and set up billing on that project. Any examples in this guide will be part of the GCP "always free" tier.
`
[Doc Terraform](https://registry.terraform.io/providers/hashicorp/google/latest/docs/guides/getting_started)


# Application
The application has four endpoints.

* GET /files : Get a list of files on google storage, and has query params `bucketName`
* POST /files : Create a file on google storage, and has query params `bucketName` e como corpo da request '{"fileName":"test123", "fileContent": "2134"}'
* GET /pods :  List the pods, query params `namespace`
* GET /messages : Show the consumeds messages

# Kubernetes
The cluster was designed to be small, just to run some applications. The cluster has maximum 10 workers nodes, and 1 initial.

## HELM
The kubernetes manifests was created with helm charts, the created objects were:
* A Deployment
* A Service and Ingress (To remote access)
* A HPA (Autoscaling aplication)
* A Service Account, Role and Rolebinding (Creating RBAC)
* A Configmap

For more envrionment just add more one helm values in the helm dir `application/manifests/sword-health`.

### How do i make sure that security is working?
#### VPC
It was create firewall rules just allow limited ports(443, 80, 22) as ingress.

#### Bucket List, Get and Write
It was create a bind with application service account with right permissions, just write for a bucket as list and get for another.

#### Kubernetes
It was created a RBAC, just authorize get, list, and watch pod in the same namespace that application is running.

### PubSub
It was create a bind with application service account with right permissions, just consuming the messages.

# Commands
### GCloud SKD
First you need to find your billing account id. Open up Cloud Shell and run:
```sh
gcloud alpha billing accounts list
```
and 
```sh
export BILLING_ACCOUNT_ID=YOUR-BILLING-ACCOUNT-ID
```
The billing account will be of the form 0X0X0X-0X0X0X-0X0X0X. Then, create projects by running:
Variable PROJECT is the name of project that you want to create.
```sh
export PROJECT=the_project_name
```
Then create a entire project: 
```sh
make build-project
```
The output of command it'll be like that:

![build_project_output](./assets/build_project_output.png)

Make a note the terraform state bucket, this will be needed.

This command will create project, terraform states bucket, terraform service account and download terraform service account to root dir.

Export GOOGLE_APPLICATION_CREDENTIALS variable to terraform use during performance.
```sh
export GOOGLE_APPLICATION_CREDENTIALS=${PWD}/${PROJECT}-service-account.json
```
```sh
export PROJECT_ID=$(make get-project-id)
```


### Environmet
After creating the project, the next step is to create the environment where the application will run. Before creating, indicate the bucket to which terraform states will be saved which is in the **backend.tf** file which is in the infrastructure/production/ directory.

Remember that at the end of apply there should be three output values: application_buckets_names, application_buckets_names and application_pubsub_subscription_paths, all of which must be added to the helm chart in the application, must be saved for future use.

Now let's actually create the environment, run the following command:

```sh
make build-environment
```
The output of command it'll be like that:

![build_deploy_output](./assets/build_deploy_output.png)

This command will create apply the terraform, creating:
* A vpc 
* Two subnetes
* A cluster gke
* Two bucket for application access
* A topic on pubsub resource.

### Application
Before deploy application it is necessary make a update in helme value, that is in application/manifests/sword-health/production.values.yaml and change:
serviceAccount.annotation.iam.gke.io/gcp-service-account and all envVar that the values was showed in the last command, **make build-environment**.
To deploy application run:

```sh
make deploy-application
```
This command will gcr credentials, push docker image to repository and install helm chart.



## Test
**Wait for ingress get ip util start de test.**

First let's get application ip:

```sh
cd application; export APPLICATION_IP=$(make get-application-ip); cd -
```

### Bucket just write
```sh
curl -X POST http://$APPLICATION_IP/files?bucketName=BUCKET_NAME -d '{"fileName":"test123", "fileContent": "2134"}' -H "Content-Type: application/json" -H "Host: sword-health.com"
```

Raplace BUCKET_NAME by real bucket name. 

Remember that when try to create the file with the same name with existed file, first gcp gonna try delete the old, then this request gonna faill, cause this service account has only permission to create file and not delete. In this case just BUCKET_WRITE_NAME will save. 

### Bucket just list and get
```sh
curl http://$APPLICATION_IP/files?bucketName=BUCKET_NAME -H "Host: sword-health.com"
```
In this case just BUCKET_LIST_GET_NAME will list. 

### Get messages from PubSub Topic

```sh
curl http://$APPLICATION_IP/messages -H "Host: sword-health.com"
```

You can publish message, in the application folder has a makefile rule : publish-message
```sh
cd application; make publish-message MESSAGE=put-your-message;cd -;
```
And access againd de endpoint to check if message was consumed.

### List Pod
```sh
curl http://$APPLICATION_IP/pods?namespace=NAMESPACE -H "Host: sword-health.com"
```
For this test has just two namespace: default and kube-system. The application was deployed into kube-system, so it should list just kube-system.

### Run all tests
```python3
python3 main.py BUCKET_LIST_GET_NAME BUCKET_WRITE_NAME 
```

# Destroy Test
To destroy all created dependencies just run:

```sh
make destroy-all
```

It will destroy terraform dependecies and created project by gcloud sdk. 

# Wrapp Up

## What whould i do if i had more time?
* Create a pipeline to deploy application
* If the project was create through a organization, i will create project using Terraform.
