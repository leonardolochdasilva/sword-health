import requests

from helpers import get_application_ip, get_base_headers

def test_should_read_pubsub_messages():
    response = requests.get(f"http://{get_application_ip()}/messages",headers=get_base_headers())
    assert response.status_code == 200
    print("PASSED: test_should_read_pubsub_messages")

def main():
    test_should_read_pubsub_messages()