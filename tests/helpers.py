import subprocess

# APPLICATION_IP = subprocess.run(['make', 'get-application-ip'], stdout=subprocess.PIPE).stdout.decode('ascii')
# print(f"Application IP: {APPLICATION_IP}")
# HEADERS = {"host": "sword-health.com"}
def get_application_ip():
    return subprocess.run(['make', 'get-application-ip'], stdout=subprocess.PIPE).stdout.decode('ascii')

def get_base_headers():
    return {"host": "sword-health.com"}