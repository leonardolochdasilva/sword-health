import requests

from helpers import get_application_ip, get_base_headers

def test_should_list_pod_on_kube_system_namespace():
    namespace = "kube-system"
    response = requests.get(f"http://{get_application_ip()}/pods?namespace={namespace}",headers=get_base_headers())
    assert response.status_code == 200
    print("PASSED: test_should_list_pod_on_kube_system_namespace")

def test_should_list_pod_on_default_namespace():
    namespace = "default"
    response = requests.get(f"http://{get_application_ip()}/pods?namespace={namespace}",headers=get_base_headers())
    assert response.status_code == 403
    print("PASSED: test_should_list_pod_on_default_namespace")

def main():
    test_should_list_pod_on_kube_system_namespace()
    test_should_list_pod_on_default_namespace()