import kubernetes
import pubsub
import storage

import sys


if __name__ == "__main__":
    just_list_get_bucket_name = sys.argv[1]
    just_write_bucket_name = sys.argv[2]
    pubsub.main()
    storage.main(just_list_get_bucket_name=just_list_get_bucket_name,just_write_bucket_name=just_write_bucket_name)  
    kubernetes.main()
