import requests
from random import random

from helpers import get_application_ip, get_base_headers


def test_should_list_object_on_bucket_list_and_get(just_list_get_bucket_name: str):
    response = requests.get(f"http://{get_application_ip()}/files?bucketName={just_list_get_bucket_name}",headers=get_base_headers())
    assert response.status_code == 200
    print("PASSED: test_should_list_object_on_bucket_list_and_get")

def test_should_not_list_object_on_bucket_write(just_write_bucket_name: str):
    response = requests.get(f"http://{get_application_ip()}/files?bucketName={just_write_bucket_name}",headers=get_base_headers())
    assert response.status_code == 403
    print("PASSED: test_should_not_list_object_on_bucket_write")

def test_should_write_file_on_bucket_write(just_write_bucket_name: str):
    file_name_suffix = random()
    body = {"fileName": f"test-{file_name_suffix}.txt", "fileContent": "123"}
    response = requests.post(f"http://{get_application_ip()}/files?bucketName={just_write_bucket_name}",headers=get_base_headers(), json=body)
    assert response.status_code == 200
    print("PASSED: test_should_write_file_on_bucket_write")

def test_should_not_write_file_on_bucket_list_get(just_list_get_bucket_name: str):
    file_name_suffix = random()
    body = {f"fileName": f"test-{file_name_suffix}.txt", "fileContent": "123"}
    response = requests.post(f"http://{get_application_ip()}/files?bucketName={just_list_get_bucket_name}",headers=get_base_headers(), json=body)
    assert response.status_code == 403
    print("PASSED: test_should_not_write_file_on_bucket_list_get")

def main(just_list_get_bucket_name:str, just_write_bucket_name: str):
    test_should_list_object_on_bucket_list_and_get(just_list_get_bucket_name=just_list_get_bucket_name)
    test_should_not_list_object_on_bucket_write(just_write_bucket_name=just_write_bucket_name)
    test_should_write_file_on_bucket_write(just_write_bucket_name=just_write_bucket_name)
    test_should_not_write_file_on_bucket_list_get(just_list_get_bucket_name=just_list_get_bucket_name)
