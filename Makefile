######################### ARGS ###############################
ENVRIONMENT=production

# GCP
GCP_SDK_VERSION=361.0.0
DEFAULT_GCP_REGION=us-west2
DEFAULT_GKE_CLUSTER_NAME=gke-cluster

# TERRAFORM
TERRAFORM_SA_NAME=terraform
TERRAFORM_VERSION=1.0.9
TERRAFORM_STATES_BUCKET:=terraform-states-$(shell bash -c 'echo $$RANDOM')

###############################################################
######################### Dependencies ########################

install-gcp-sdk:
# SUDO
	@echo "Installing GCP SKD..."
	curl -O https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-sdk-${GCP_SDK_VERSION}-linux-arm.tar.gz
	tar xvzf  google-cloud-sdk-${GCP_SDK_VERSION}-linux-arm.tar.gz
	./google-cloud-sdk/install.shPROJECTPROJECTPROJECT
	@echo "GCP SKD ${GCP_SDK_VERSION}: Installation completed"

install-terraform:
# SUDO
	@echo "Installing Terraform..."
	curl https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip --output terraform.zip
	unzip terraform.zip
	mv terraform /usr/local/bin
	@echo "Terraform ${TERRAFORM_VERSION}: Installation completed"

install-deps: install-gcp-sdk install-terraform

###############################################################
####################### GCLOUD ################################

create-gcp-project:
	@echo "Creating project: ${PROJECT}"
	PROJECT_ID=${PROJECT}-$(shell bash -c 'echo $$RANDOM'); \
	gcloud projects create $${PROJECT_ID} --name=${PROJECT}; \
	gcloud beta billing projects link $${PROJECT_ID} --billing-account=${BILLING_ACCOUNT_ID}

delete-gcp-project:
	@echo "Deleting project: ${PROJECT}"
	gcloud projects delete $$(make --no-print-directory get-project-id)

create-terraform-states-bucket:
	@echo "Creating terraform states bucket: ${TERRAFORM_STATES_BUCKET}"
	PROJECT_ID=$$(make --no-print-directory get-project-id); \
	echo $${PROJECT_ID}; \
	gsutil mb  -p $${PROJECT_ID} gs://${TERRAFORM_STATES_BUCKET};

create-terraform-service-account:
	@echo "Creating terraform service account"
	PROJECT_ID=$$(make --no-print-directory get-project-id); \
	gcloud iam service-accounts create ${TERRAFORM_SA_NAME} --display-name="Terraform Service Account" --project $${PROJECT_ID}; \
	gcloud projects add-iam-policy-binding $${PROJECT_ID} --member=serviceAccount:${TERRAFORM_SA_NAME}@$${PROJECT_ID}.iam.gserviceaccount.com --role='roles/editor'; \
	gcloud projects add-iam-policy-binding $${PROJECT_ID} --member=serviceAccount:${TERRAFORM_SA_NAME}@$${PROJECT_ID}.iam.gserviceaccount.com --role='roles/resourcemanager.projectIamAdmin'; \
	gcloud projects add-iam-policy-binding $${PROJECT_ID} --member=serviceAccount:${TERRAFORM_SA_NAME}@$${PROJECT_ID}.iam.gserviceaccount.com --role='roles/pubsub.admin'; \
	gcloud projects add-iam-policy-binding $${PROJECT_ID} --member=serviceAccount:${TERRAFORM_SA_NAME}@$${PROJECT_ID}.iam.gserviceaccount.com --role='roles/iam.serviceAccountAdmin';

get-terraform-service-account-key:
# PROJECT_ID=$$(gcloud projects list | grep $${PROJECT} | awk '{print $$1}');
	PROJECT_ID=$$(make --no-print-directory get-project-id); \
	gcloud iam service-accounts keys create ${PWD}/$${PROJECT}-service-account.json --iam-account=${TERRAFORM_SA_NAME}@$${PROJECT_ID}.iam.gserviceaccount.com;

get-project-id:
	@gcloud projects list --filter="name:${PROJECT}" | grep ${PROJECT} | awk '{print $$1}'

build-project: create-gcp-project create-terraform-states-bucket create-terraform-service-account get-terraform-service-account-key
	@echo ">>> Terraform states bucket: ${TERRAFORM_STATES_BUCKET}"
	@echo ">>> Terraform Service Account File: ${PROJECT}-service-account.json"
	@echo ">>> Project Id: $$(make --no-print-directory get-project-id)"

##################################################################
####################### Kubernetes ###############################

### CLUSTER CREDENTIONS
get-k8s-cluster-credentials:
	@echo "Getting K8s cluster credentials"
	PROJECT_ID=$$(make get-project-id) \
	gcloud container clusters get-credentials ${DEFAULT_GKE_CLUSTER_NAME} --region ${DEFAULT_GCP_REGION} --project $${PROJECT_ID}

### NGINX INGRESS
install-ingress-controller:
	@echo "Installing nginx ingress controller"
	kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v1.0.4/deploy/static/provider/cloud/deploy.yaml

##################################################################
######################### ENVRIONMENT ############################

build-environment:
	cd infrastructure; \
	make terraform-init; \
	make terraform-apply ARGS=-auto-approve; \
	cd - ; \
	make get-k8s-cluster-credentials; \
	make install-ingress-controller; \
	cd infrastructure; \
	make terraform-output; \
	cd -

##################################################################
########################## APPLICATION ###########################

deploy-application:
	cd application; \
	make get-gcp-docker-credentials; \
	make push-image; \
	make install-chart

##################################################################
destroy-all:
	PROJECT_ID=$$(make get-project-id) \
	cd infrastructure; \
	make terraform-destroy;
	make delete-gcp-project

##################################################################